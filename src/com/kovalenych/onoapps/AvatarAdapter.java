package com.kovalenych.onoapps;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.androidquery.AQuery;

import java.util.ArrayList;

public class AvatarAdapter extends ArrayAdapter<String> {
    private final Context context;
    private ArrayList<String> urls;
    private AQuery aQuery;

    public AvatarAdapter(Context context, ArrayList<String> urls, AQuery aQuery) {
        super(context, R.layout.item_ava, urls);
        this.context = context;
        this.urls = urls;
        this.aQuery = aQuery;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.item_ava, null, true);
            holder = new ViewHolder();
            holder.avatar = (ImageView) rowView.findViewById(R.id.avatar_pic);
            holder.name = (TextView) rowView.findViewById(R.id.avatar_name);

            holder.background = (RelativeLayout) rowView.findViewById(R.id.avatar_item_bg);

            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        if (position % 2 == 0)
            holder.background.setBackgroundResource(R.drawable.bg);
        else
            holder.background.setBackgroundResource(R.drawable.bg_square);
        holder.name.setText("Avatar №" + (position+1));

        aQuery.id(holder.avatar).image(urls.get(position));

        return rowView;
    }

    static class ViewHolder {
        public ImageView avatar;
        public TextView name;
        public RelativeLayout background;
    }

}