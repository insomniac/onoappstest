package com.kovalenych.onoapps;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.androidquery.AQuery;

import java.util.ArrayList;

public class AvatarDialog extends Dialog {

    private Context _context;
    private AQuery aquery;
    ArrayList<String> picUrls;

    public AvatarDialog(Context context, AQuery aQuery) {
        super(context);
        this._context = context;
        this.aquery = aQuery;
        setContentView(R.layout.avatardialog);
        ListView avatarList = (ListView) findViewById(R.id.avatar_list);
        picUrls = new ArrayList<String>();
        for (int i = 1; i < 10; i++)
            picUrls.add("http://onoapps.com/Dev/avatars/a0" + i + ".png");
        for (int i = 10; i < 14; i++)
            picUrls.add("http://onoapps.com/Dev/avatars/a" + i + ".png");

        avatarList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                aquery.id(R.id.avatarview).image(picUrls.get(i));
                ((Main) _context).onAvatarSelected(picUrls.get(i));
                dismiss();
            }
        });

        avatarList.setAdapter(new AvatarAdapter(context, picUrls, aQuery));
        setTitle(context.getString(R.string.choose_ava));
    }
}
