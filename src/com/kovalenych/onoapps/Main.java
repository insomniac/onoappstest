package com.kovalenych.onoapps;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main extends Activity {

    public static final String RINGTONES = "ringtones";
    private ArrayList<JSONObject> tones = new ArrayList<JSONObject>();
    public String chosenAvatar;
    public String chosenRingtone;
    AQuery aq;

    EditText nameEdit;
    EditText phoneEdit;
    EditText surnameEdit;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        aq = new AQuery(this);

        nameEdit = (EditText) findViewById(R.id.name);
        surnameEdit = (EditText) findViewById(R.id.sur_name);
        phoneEdit = (EditText) findViewById(R.id.phone);
        findViewById(R.id.avatarview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog avatarPicker = new AvatarDialog(Main.this, aq);
                avatarPicker.show();
            }
        });

        Button chooseRingtone = (Button) findViewById(R.id.choose_ringtone);
        chooseRingtone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog ringtoneDialog = new RingtoneDialog(Main.this, tones);
                ringtoneDialog.setTitle(getString(R.string.choose_ringtone));
                ringtoneDialog.show();
            }
        });

        aq.ajax(URL, JSONObject.class, this, "jsonCallback");

        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (haveInternet())
                    share();
                else
                    Toast.makeText(Main.this,"No connection",Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean haveInternet() {
        NetworkInfo info = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            // here is the roaming option you can change it if you want to disable internet while roaming, just return false
            return true;
        }
        return true;
    }

    private void share() {
        String name = nameEdit.getText().toString();
        String surname = surnameEdit.getText().toString();
        String phone = phoneEdit.getText().toString();

        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(name).append(" ").append(surname).append("\n");
        builder.append("Phone: ").append(phone).append("\n");
        if (chosenRingtone != null)
            builder.append("ringtone: ").append(chosenRingtone).append("\n");
        if (chosenAvatar != null)
            builder.append("avatar: ").append(chosenAvatar).append("\n");

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"welcome@onoapps.com",});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share contact");
        intent.putExtra(Intent.EXTRA_TEXT, builder.toString());

        startActivity(Intent.createChooser(intent, "Send e-mail"));
    }


    public void onAvatarSelected(String avatar) {
        chosenAvatar = avatar;
    }

    public static String URL = "http://onoapps.com/Dev/ringtones.txt";

    class RingtoneDialog extends Dialog {

        public RingtoneDialog(Context context, ArrayList<JSONObject> rings) {
            super(context);
            setContentView(R.layout.avatardialog);
            ListView ringtoneList = (ListView) findViewById(R.id.avatar_list);
            ringtoneList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        chosenRingtone = tones.get(i).getString("ringName");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dismiss();
                }
            });

            ringtoneList.setAdapter(new ToneAdapter(context, rings));
        }
    }


    public void jsonCallback(String url, JSONObject json, AjaxStatus status) {

        if (json != null) {

            try {
                JSONArray ringtones = json.getJSONArray(RINGTONES);
                for (int i = 0, end = ringtones.length(); i < end; i++) {
                    JSONObject twittJson = ringtones.getJSONObject(i);
                    tones.add(twittJson);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }
}
