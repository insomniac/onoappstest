package com.kovalenych.onoapps;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ToneAdapter extends ArrayAdapter<JSONObject> {
    private final Context context;
    private ArrayList<JSONObject> ringTones;

    public ToneAdapter(Context context, ArrayList<JSONObject> twits) {
        super(context, R.layout.item_ring, twits);
        this.context = context;
        this.ringTones = twits;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.item_ring, null, true);
            holder = new ViewHolder();
            holder.name = (TextView) rowView.findViewById(R.id.name);
            holder.desc = (TextView) rowView.findViewById(R.id.desc);
            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        try {
            holder.name.setText(ringTones.get(position).getString("ringName"));
            holder.desc.setText(ringTones.get(position).getString("ringDec"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rowView;
    }

    static class ViewHolder {
        public TextView name;
        public TextView desc;
    }

}